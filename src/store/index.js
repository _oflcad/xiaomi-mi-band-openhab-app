import {createStore, compose, applyMiddleware} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, autoRehydrate} from 'redux-persist';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import reducer from '../reducers';
import AppState from '../reducers/App/state';
import ConnectState from '../reducers/Connect/state';
import rootSaga from '../saga';

const sagaMiddleware = createSagaMiddleware();

const defaultState = {
  app: AppState,
  connect: ConnectState,
};
export const configureStore = (initialState = defaultState, action) => {
  var store = createStore(
    reducer,
    initialState,
    compose(
      autoRehydrate(),
      applyMiddleware(sagaMiddleware, thunk),
    ),
  );
  sagaMiddleware.run(rootSaga);
  /* const asyncStorageKeys = AsyncStorage.getAllKeys(); ///<==========================================================Purging Storage in IOS request loading in the first time.
  if (asyncStorageKeys.length > 0) {
    AsyncStorage.clear();
  } */
 /*  persistStore(store, {
    storage: AsyncStorage,
  }); */

  return store;
};

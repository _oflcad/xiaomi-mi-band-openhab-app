import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {withNavigation} from 'react-navigation';
import {Layout} from '../../Helpers';
const Header = ({navigation}) => {
  const _navigate = () => {
    navigation.navigate('Home');
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={_navigate}>
        <Image
          source={require('../../assets/Images/back_arrow.png')}
          style={styles.backIcon}
        />
      </TouchableOpacity>
      <View style={styles.middleContainer}>
        <Text style={styles.title}>OpenHab</Text>
        <Image
          source={require('../../assets/Images/openhabLogo.png')}
          style={styles.backIcon}
        />
      </View>

      <View />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Layout.window.width,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingHorizontal: 10,
  },
  title: {
    fontFamily: 'Comfortaa-Bold',
    color: '#ff6b81',
    fontSize: 20,
    marginBottom: 5,
    marginRight: 5,
  },
  backIcon: {
    width: 25,
    height: 25,
  },
  middleContainer: {
    flexDirection: 'row',
  },
});

export default withNavigation(Header);

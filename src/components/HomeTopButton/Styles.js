import {StyleSheet} from 'react-native';
import {Layout} from '../../Helpers';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: -40,
    left: 0,
    right: 0,
    width: Layout.window.width - 20,
    height: 80,
    borderRadius: 20,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: 'white',
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  title: {
    fontFamily: 'MontserratAlternates-Bold',
    fontSize: 20,
    color: '#000',
    marginHorizontal: 10,
    textAlign: 'auto',
  },
  icon: {
    marginHorizontal: 10,
  },
  coords: {
    fontFamily: 'MontserratAlternates-Bold',
    fontSize: 20,
    color: '#000',
    marginHorizontal: 10,
    textAlign: 'auto',
    marginHorizontal: 10,
  },

  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

import React from 'react';
import {View, Text} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Styles from './Styles';
const HomeTopButton = () => {
  return (
    <View style={Styles.container}>
      <View style={Styles.row}>
      <View style={Styles.icon}>
        <FontAwesome
          size={40}
          color={'#f65009'}
          name="home"
        />
        </View>
        <Text style={Styles.title}>Home</Text>
      </View>
      <View style={Styles.row}>
        <Text style={Styles.coords}>35,435354</Text>
        <Text style={Styles.coords}>35,435354</Text>
      </View>
    </View>
  );
};

export default HomeTopButton;

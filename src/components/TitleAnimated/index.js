import React, {useState, useEffect} from 'react';
import {Text, View, Animated, StyleSheet} from 'react-native';
import {withNavigation} from 'react-navigation';
const Title = ({navigation}) => {
  const [fadeValue, setFadeValue] = useState(new Animated.Value(0));
  useEffect(() => {
    setTimeout(() => {
      _start();
    }, 2000);
  });
  const _start = () => {
    Animated.timing(fadeValue, {
      toValue: 1,
      duration: 1000,
    }).start(() => {
      navigation.navigate('Home');
    });
  };
  return (
    <Animated.View style={[styles.container, {opacity: fadeValue}]}>
      <Text style={styles.text}>Mi Heart</Text>
    </Animated.View>
  );
};
export default withNavigation(Title);
const styles = StyleSheet.create({
  container: {
    opacity: 0,
    margin: 5,
    borderRadius: 12,
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'Comfortaa-Bold',
    fontSize: 30,
    color: '#f65009',
    textAlign: 'center',
  },
});

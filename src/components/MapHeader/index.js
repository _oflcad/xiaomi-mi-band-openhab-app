import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import {Layout} from '../../Helpers';
import Geolocation from '@react-native-community/geolocation';

const MapHeader = () => {
  return (
    <MapView
      customMapStyle={Layout.mapStyle}
      initialRegion={{
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }}
      style={{flex: 1, ...StyleSheet.absoluteFillObject}}
      showsMyLocationButton
      showsUserLocation
    />
  );
};

export default MapHeader;

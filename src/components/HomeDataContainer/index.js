import React from 'react';
import {View, Text, Image} from 'react-native';
import Styles from './Styles';

const DataContainer = ({imageUrl, value, unit, title}) => {
  return (
    <View style={Styles.container}>
      <View style={Styles.iconContainer}>
        <Image source={imageUrl} style={Styles.icon} />
      </View>
      <View style={Styles.titleContainer}>
      <Text style={Styles.title}>{title}</Text>
      </View>
      <View style={Styles.valueContainer}>
        <Text style={Styles.value}>{value}</Text>
      </View>
    </View>
  );
};

export default DataContainer;

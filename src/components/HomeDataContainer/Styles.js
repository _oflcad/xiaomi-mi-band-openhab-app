import {StyleSheet} from 'react-native';
import {Layout} from '../../Helpers';

export default StyleSheet.create({
  container: {
    width: Layout.window.width - 20,
    height: 60,
    borderRadius: 20,
    backgroundColor: 'white',
    borderColor: '#f65009',
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.51,
    shadowRadius: 13.16,
    elevation: 20,
  },
  iconContainer: {
    flex: 1,
    alignItems: 'flex-start',
    padding: 10,
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  valueContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 25,
    color: 'black',
    textAlign: 'center',
  },
  value: {
    fontSize: 25,
    color: '#f65009',
    textAlign: 'center',
  },
  icon: {
    width: 30,
    height: 30
  }
});

import {put, takeLatest, all} from 'redux-saga/effects';
import * as Types from '../actions/Types';
import axios from 'axios';

const config = {
  headers: {'Content-Type': 'text/plain', Accept: 'Application/json'},
};

function* persistRssi(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/rssi', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST RSSI' + response.status);
      });
    yield put({type: Types.SET_RSSI_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_RSSI_FAILURE});
  }
}

function* persistBattery(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/battery', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST BATTERY' + response.status);
      });
    yield put({type: Types.SET_BATT_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_BATT_FAILURE});
  }
}

function* persistFirmware(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/fVersion', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST FIRMWARE' + response.status);
      });
    yield put({type: Types.SET_FIRMWARE_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_FIRMWARE_FAILURE});
  }
}

function* persistSerial(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/Snumber', val, config)
      .then(response => {
        console.log(
          'STATUS RESPONSE FROM PERSIST SERIAL NUMBER' + response.status,
        );
      });
    yield put({type: Types.SET_SERIAL_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_STEPS_FAILURE});
  }
}

function* persistTime(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/time', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST TIME' + response.status);
      });
    yield put({type: Types.SET_TIME_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_TIME_FAILURE});
  }
}

function* persistSteps(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/steps', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST STEPS' + response.status);
      });
    yield put({type: Types.SET_STEPS_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_STEPS_FAILURE});
  }
}

function* persistCalories(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/calory', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST CALORIES' + response.status);
      });
    yield put({type: Types.SET_CALORIES_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_CALORIES_FAILURE});
  }
}

function* persistDistance(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/distance', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST DISTANCE' + response.status);
      });
    yield put({type: Types.SET_DISTANCE_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_DISTANCE_FAILURE});
  }
}

function* persistHeartRate(payload) {
  let {val, URL} = payload;
  try {
    yield axios
      .post('http://' + URL + ':8080/rest/items/heartbeat', val, config)
      .then(response => {
        console.log('STATUS RESPONSE FROM PERSIST RSSI' + response.status);
      });
    yield put({type: Types.SET_HEARRATE_SUCCESS, val});
  } catch (error) {
    yield put({type: Types.SET_HEARRATE_FAILURE});
  }
}

function* actionWatcher() {
  yield takeLatest(Types.SET_RSSI, persistRssi);
  yield takeLatest(Types.SET_BATT, persistBattery);
  yield takeLatest(Types.SET_FIRMWARE, persistFirmware);
  yield takeLatest(Types.SET_SERIAL, persistSerial);
  yield takeLatest(Types.SET_TIME, persistTime);
  yield takeLatest(Types.SET_STEPS, persistSteps);
  yield takeLatest(Types.SET_CALORIES, persistCalories);
  yield takeLatest(Types.SET_DISTANCE, persistDistance);
  yield takeLatest(Types.SET_HEARRATE, persistHeartRate);
}
export default function* rootSaga() {
  yield all([actionWatcher()]);
}

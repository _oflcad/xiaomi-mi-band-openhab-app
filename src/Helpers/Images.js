export default {
  heartAnimation: require('../assets/Animations/healthy-heart.json'),
  distance: require('../assets/Images/distance.png'),
  steps: require('../assets/Images/steps.png'),
  calory: require('../assets/Images/calories.png'),
  heartBeat: require('../assets/Images/heartbeat.png'),
};

export const parseDate = ($buff) => {
  let year = $buff.readUInt16LE(0),
    mon = $buff[2] - 1,
    day = $buff[3],
    hrs = $buff[4],
    min = $buff[5],
    sec = $buff[6],
    msec = ($buff[8] * 1000) / 256;
  var today = new Date(year, mon, day, hrs, min, sec);
  return today.toLocaleTimeString();
};

import * as Types from './Types';
import Axios from 'axios';
import NavigationServices from '../../Services/NavigationServices';
import {NavigationActions} from 'react-navigation';
const URL = 'https://pepapps.com/xiaomi-my-heart/public/api/auth/login';
const secondUrl = 'https://pepapps.com/xiaomi-my-heart/public/api/auth/logout';
export const authenticateUser = (email, password) => {
  return (dispatch) => {
    dispatch({type: Types.LOADING});

    let formdata = new FormData();
    //console.log('value are', email, password)
    formdata.append('email', email);
    formdata.append('password', password);
    Axios({
      method: 'POST',
      url: URL,
      data: formdata,
    })
      .then((res) => {
        console.log('AUTH RESPONSE', res.data.access_token);
        let token = res.data.access_token;
        let username = email;
        dispatch({type: Types.AUTHENTICATE_SUCCESS, token, username, password});
        NavigationServices.navigate('Profile');
      })
      .catch((error) => {
        dispatch({type: Types.AUTHENTICATE_FAILURE});
        console.log('Error from auth', error);
      });
  };
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const {token, username, password} = getState().auth;
    let formdata = new FormData();
    //console.log('value are', email, password)
    formdata.append('email', username);
    formdata.append('password', password);
    let myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${token}`);
    Axios({
      method: 'POST',
      url: secondUrl,
      data: formdata,
      headers: myHeaders,
    })
      .then((res) => {
        dispatch({type: Types.LOGOUT_SUCCESS});
      })
      .catch((error) => {
        dispatch({type: Types.LOGOUT_FAILURE});
      });
  };
};

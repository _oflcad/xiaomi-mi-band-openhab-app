export default {
  isAuthenticated: false,
  isLoading: false,
  user: {},
  username: null,
  password: null,
  token: null,
  authError: null,
};

import * as Types from './Types';

export const $setRssiM = val => {
  return dispatch => {
    dispatch({type: Types.SET_RSSI, val});
  };
};

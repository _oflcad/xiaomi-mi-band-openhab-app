import * as Types from './Types';
import axios from 'axios';
const URL =
  'https://pepapps.com/xiaomi-my-heart/public/api/Y1ciz/hana-doc/postdata';

export const scanning = (val) => {
  return (dispatch) => {
    dispatch({type: Types.SCANNING, val});
  };
};

export const $setRssi = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_RSSI_SUCCESS, val});
  };
};

export const $setBattery = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_BATT_SUCCESS, val});
  };
};

export const $setFirmware = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_FIRMWARE_SUCCESS, val});
  };
};

export const $setSerial = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_SERIAL_SUCCESS, val});
  };
};

export const $setTime = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_TIME_SUCCESS, val});
  };
};

export const $setSteps = (val) => {
  return (dispatch) => {
    dispatch({type: Types.SET_STEPS_SUCCESS, val});
  };
};

export const $setDistance = (val) => {
  return (dispatch, getState) => {
    dispatch({type: Types.SET_DISTANCE_SUCCESS, val});
  };
};

export const $setCalories = (val) => {
  return (dispatch, getState) => {
    const URL = getState().connect.URL;
    dispatch({type: Types.SET_CALORIES_SUCCESS, val});
  };
};

export const $setHearRate = (val) => {
  return (dispatch) => {
    dispatch({type: Types.SET_HEARTRATE_SUCCESS, val});
  };
};

export const $persistData = () => {
  return (dispatch, getState) => {
    const {battery, steps, distance, calory, heartbeat} = getState().app;
    const {token} = getState().auth;

    let formdata = new FormData();
    let myHeaders = new Headers();
    myHeaders.append('Authorization', `Bearer ${token}`);
    formdata.append('steps', steps);
    formdata.append('distances', distance);
    formdata.append('calory', calory);
    formdata.append('heartbeat', heartbeat);
    formdata.append('lat', '20.1418732');
    formdata.append('lng', '60.147758');
    axios({
      method: 'POST',
      headers: myHeaders,
      data: formdata,
      url: URL,
    })
      .then((res) => {
        dispatch({type: Types.PERSIST_DATA_SUCCESS});
      })
      .catch((error) => {
        dispatch({type: Types.PERSIST_DATA_FAILURE});
      });
  };
};

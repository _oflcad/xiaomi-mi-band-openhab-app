import * as Types from './Types';
export const $setIP = ip => {
  return dispatch => {
    dispatch({type: Types.SET_URL_OPENHAB, ip});
  };
};

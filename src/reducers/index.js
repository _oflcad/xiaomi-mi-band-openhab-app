import {combineReducers} from 'redux';

import AppReducer from './App';
import ConnectReducer from './Connect';
import AuthReducer from './Auth';

const reducers = combineReducers({
  app: AppReducer,
  connect: ConnectReducer,
  auth: AuthReducer,
});

export default reducers;

import * as Types from '../../actions/Types';
import defaultState from './state';
module.exports = (state = defaultState, action) => {
  switch (action.type) {
    case Types.SET_URL_OPENHAB:
      return {
        ...state,
        URL: action.ip,
      };
    default:
      return state;
  }
};

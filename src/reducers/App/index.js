import * as Types from '../../actions/Types';
import defaultState from './state';
module.exports = (state = defaultState, action) => {
  switch (action.type) {
    case Types.SCANNING:
      return {
        ...state,
        scanning: !action.val,
      };
    case Types.SET_RSSI_SUCCESS:
      return {
        ...state,
        rssi: action.val,
      };
    case Types.SET_RSSI_FAILURE:
      return state;
    case Types.SET_BATT_SUCCESS:
      return {
        ...state,
        battery: action.val,
      };
    case Types.SET_BATT_FAILURE:
      return state;
    case Types.SET_FIRMWARE_SUCCESS:
      return {
        ...state,
        fVersion: action.val,
      };
    case Types.SET_FIRMWARE_FAILURE:
      return state;
    case Types.SET_SERIAL_SUCCESS:
      return {
        ...state,
        Snumber: action.val,
      };
    case Types.SET_SERIAL_FAILURE:
      return state;
    case Types.SET_TIME_SUCCESS:
      return {
        ...state,
        time: action.val,
      };
    case Types.SET_TIME_FAILURE:
      return state;
    case Types.SET_STEPS_SUCCESS:
      return {
        ...state,
        steps: action.val,
      };
    case Types.SET_STEPS_FAILURE:
      return state;
    case Types.SET_DISTANCE_SUCCESS:
      return {
        ...state,
        distance: action.val,
      };
    case Types.SET_DISTANCE_FAILURE:
      return state;
    case Types.SET_CALORIES_SUCCESS:
      return {
        ...state,
        calory: action.val,
      };
    case Types.SET_CALORIES_FAILURE:
      return state;
    case Types.SET_HEARTRATE_SUCCESS:
      return {
        ...state,
        heartbeat: action.val,
      };
    case Types.SET_HEARTRATE_FAILURE:
      return state;
    case Types.PERSIST_DATA_SUCCESS:
      return state;
    case Types.PERSIST_DATA_FAILURE:
      return state;
    default:
      return state;
  }
};

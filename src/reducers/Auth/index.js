import {Types, State} from '../../actions/Auth';

module.exports = (state = State, action) => {
  switch (action.type) {
    case Types.LOADING:
      return {
        ...state,
        isLoading: true,
        authError: null,
      };
    case Types.AUTHENTICATE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        authError: null,
        token: action.token,
        username: action.username,
        password: action.password,
      };
    case Types.AUTHENTICATE_FAILURE:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        authError: 'Please Check Credentials',
      };
     case Types.LOGOUT_SUCCESS:
       return {
         ...state,
         isLoading: false,
        isAuthenticated: false,
        token: null,
        username: null,
        password: null
       } 
    default:
      return state;
  }
};

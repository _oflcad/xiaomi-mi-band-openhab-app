import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import HomeScreen from '../screens/Home';
import ConnectScreen from '../screens/Connect';
import SplashScreen from '../screens/Splash';
import SignInScreen from '../screens/Auth/SignIn';
import ProfileScreen from '../screens/Auth/Profile';

import MyDoctorScreen from '../screens/MyDoctor';



const authStack = createSwitchNavigator(
  {
    Profile: ProfileScreen,
    SignIn: SignInScreen,
  },
  {
    initialRouteName: 'SignIn',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: 'hidden',
    },
  },
);

const TabNavigator = createBottomTabNavigator(
  {
    Auth: authStack,
    Home: HomeScreen,
    MyDoctor: MyDoctorScreen,
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerVisible: false,
    },
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = 'ios-heart';
        } else if (routeName === 'Auth') {
          iconName = 'ios-person';
        } else if (routeName === 'MyDoctor') {
          iconName = 'ios-medical';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#f65009',
      inactiveTintColor: 'gray',
    },
  },
);

const AppNavigator = createStackNavigator(
  {
    SplashScreen: SplashScreen,
    App: TabNavigator,
  },
  {
    initialRouteName: 'SplashScreen',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: 'hidden',
    },
  },
);

export default createAppContainer(AppNavigator);

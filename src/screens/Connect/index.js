import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  StatusBar,
  TouchableHighlight,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import {$setIP} from '../../actions/connectActions';
import Header from '../../components/Header';
const window = Dimensions.get('window');

class ConnectPage extends Component {
  static navigationOptions = {header: null};
  state = {
    value: this.props.connect.URL,
  };

  onChangeText = value => this.setState({value});

  _setOpenhabUrl = val => {
    this.props.onDispatchSetUrl(val);
    ToastAndroid.showWithGravity(
      'IP Saved',
      ToastAndroid.SHORT,
      ToastAndroid.TOP,
      25,
      100,
    );
  };

  _handleNavigation = () => this.props.navigation.navigate('Home');

  render() {
    const {value} = this.state;
    const {URL} = this.props.connect;
    return (
      <View style={styles.container}>
        <Header />

        <View style={styles.contentContainer}>
          {URL ? (
            <Text style={styles.title}> OPENHAB IP :</Text>
          ) : (
            <Text style={styles.title}> Please Provide Your Ip Address</Text>
          )}

          <TextInput
            placeholder={'OpenHab IP address '}
            style={styles.input}
            onChangeText={text => this.onChangeText(text)}
            value={value}
            autoFocus={URL ? false : true}
            keyboardType={'number-pad'}
            underlineColorAndroid={'transparent'}
          />
        </View>
        <View style={styles.center}>
          <TouchableHighlight
            onPress={() => this._setOpenhabUrl(value)}
            style={URL ? styles.buttonSucces : styles.button}>
            <Text style={styles.buttonText}>Save</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
    marginTop: StatusBar.currentHeight,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoContainer: {
    marginHorizontal: 10,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: window.width / 3,
    height: window.width / 3,
  },
  title: {
    fontFamily: 'Comfortaa-Bold',
    fontSize: 20,
    color: 'black',
    textAlign: 'center',
    marginVertical: 40,
  },
  input: {
    width: window.width - 20,
    height: 50,
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#ff6b81',
    justifyContent: 'center',
    margin: 10,
  },
  button: {
    width: window.width / 2,
    height: 50,
    marginTop: 10,
    margin: 10,
    padding: 20,
    backgroundColor: '#ff6b81',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 50,
  },
  buttonSucces: {
    width: window.width / 2,
    height: 50,
    marginTop: 10,
    margin: 10,
    padding: 20,
    backgroundColor: 'green',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 50,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'Comfortaa-Bold',
    color: 'white',
    fontSize: 20,
  },
});

const mapStateToProps = state => {
  return {
    connect: state.connect,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetUrl: val => dispatch($setIP(val)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ConnectPage);

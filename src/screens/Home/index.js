/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ScrollView,
  AppState,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import {connect} from 'react-redux';
import BleManager from 'react-native-ble-manager';
import Buffer from 'buffer';
import _ from 'lodash';
import {
  $setRssi,
  $setBattery,
  $setFirmware,
  $setSerial,
  $setTime,
  $setSteps,
  $setCalories,
  $setDistance,
  $setHearRate,
  $persistData,
} from '../../actions';
import {$setRssiM} from '../../actions/sagaAction';
import Geolocation from '@react-native-community/geolocation';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import {Layout, Helpers, Images} from '../../Helpers';
import {UUID_BASE, UUID_BASE_2} from '../../Helpers/uuid';
import MapHeader from '../../components/MapHeader';
import HomeTopButton from '../../components/HomeTopButton';
import HomeDataContainer from '../../components/HomeDataContainer';
import {parseDate} from '../../Helpers/Helpers';
class Home extends Component {
  static navigationOptions = {header: null};

  constructor() {
    super();

    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: '',
      connectedTo: 'No Device is Connected',
    };
    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(
      this,
    );
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(
      this,
    );
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    this.handlerStop = bleManagerEmitter.addListener(
      'BleManagerStopScan',
      this.handleStopScan,
    );
    this.handlerDisconnect = bleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      this.handleDisconnectedPeripheral,
    );
    this.handlerUpdate = bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      this.handleUpdateValueForCharacteristic,
    );

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ).then((result) => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.requestPermission(
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          ).then((result) => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
  }

  handleAppStateChange(nextAppState) {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUpdate(nextProps) {
    if (this.props.app !== nextProps.app) {
      return true;
    } else {
      return false;
    }
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    //let dataBuffer = Buffer.Buffer.from(data);
    console.log(
      'Received data from ' +
        data.peripheral +
        ' characteristic ' +
        data.characteristic,
      data.value,
    );

    console.log(data.characteristic, UUID_BASE_2('2a37'));
    console.log(_.isEqual(data.characteristic, UUID_BASE_2('2a37')));
    let val = data.characteristic;

    if (_.isEqual(val, UUID_BASE('0007'))) {
      let res = Buffer.Buffer.from(data.value);
      //console.log('Heart rate : ' + res.readUInt16BE(0));
      console.log('Read !! Steps Count:', res.readUInt16LE(1));
      this.props.onDispatchPersistST(res.readUInt16LE(1).toString());
      this.props.onDispatchPersistData();
      if (res.length >= 8) {
        console.log('Read !! Distance:', res.readUInt32LE(5));
        this.props.onDispatchPersistD(res.readUInt32LE(5).toString());
        this.props.onDispatchPersistData();
      }
      if (res.length >= 12) {
        console.log('Read !! Calories:', res.readUInt32LE(9));
        this.props.onDispatchPersistC(res.readUInt32LE(9).toString());
        this.props.onDispatchPersistData();
      }
    } else if (_.isEqual(val, UUID_BASE_2('2a37'))) {
      let res = Buffer.Buffer.from(data.value);
      this.props.onDispatchPersistH(data.value[1].toString());
      console.log('Heart rate : ' + data.value[1]);
      this.props.onDispatchPersistData();
    }
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({scanning: false});
  }

  startScan() {
    if (!this.state.scanning) {
      //this.setState({peripherals: new Map()});
      BleManager.scan([], 3, true).then((results) => {
        console.log('Scanning...');
        this.setState({scanning: true});
      });
    }
  }

  retrieveConnected() {
    BleManager.getConnectedPeripherals([]).then((results) => {
      if (results.length === 0) {
        console.log('No connected peripherals');
      }
      console.log(results);
      var peripherals = this.state.peripherals;
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i];
        peripheral.connected = true;
        peripherals.set(peripheral.id, peripheral);
        this.setState({peripherals});
        this.test(peripheral);
      }
    });
  }

  handleDiscoverPeripheral(peripheral) {
    var peripherals = this.state.peripherals;
    //sconsole.log('Got ble peripheral', peripheral);
    /* if (!peripheral.name) {
          peripheral.name = 'NO NAME';
        } */
    peripherals.set(peripheral.id, peripheral);
    this.setState({peripherals});
  }

  test(peripheral) {
    if (peripheral) {
      if (peripheral.connected) {
        BleManager.disconnect(peripheral.id);
      } else {
        BleManager.connect(peripheral.id)
          .then(() => {
            let peripherals = this.state.peripherals;
            let p = peripherals.get(peripheral.id);
            this.setState({connectedTo: peripheral.name});
            if (p) {
              p.connected = true;
              peripherals.set(peripheral.id, p);
              this.setState({peripherals: peripherals, peripheral: peripheral});
            }
            console.log('Connected to ' + peripheral.id);

            console.log('Starting to retreive all services');
            BleManager.retrieveServices(peripheral.id).then(() => {
              BleManager.readRSSI(peripheral.id).then((rssi) => {
                this.props.onDispatchPersistR(rssi.toString());
                console.log('Retrieved actual RSSI value', rssi);
              });
            });
            BleManager.createBond(peripheral.id)
              .then(() => {
                console.log(
                  'createBond success or there is already an existing one',
                );
              })
              .catch(() => {
                console.log('fail to bond');
              });

            this.handleBattery(peripheral);
            this.handleDeviceInfo(peripheral);
            this.handlePedometerStats(peripheral);
            this.handleHeartRate(peripheral);
          })
          .catch((error) => {
            console.log('Connection error', error);
          });
      }
    }
  }

  showNotification(peripheral) {
    console.log('Device' + peripheral.name + 'Service: 1802 && Char: 2a06');
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.write(peripheral.id, '1802', '2a06', [0x01])
            .then(() => {
              console.log('SUCCESS WRITE !! CHECK BAND FOR message ');
            })
            .catch((error) => {
              console.log('ERROR WRITE !! CHECK BAND FOR MESSAGE', error);
            });
        })
        .catch((error) => {
          console.log('Error Retrieving services', error);
        });
    }, 6000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.write(peripheral.id, '1802', '2a06', [0x02])
            .then(() => {
              console.log('SUCCESS WRITE !! CHECK BAND FOR Phone Call');
            })
            .catch((error) => {
              console.log('ERROR WRITE !! CHECK BAND FOR MESSAGE', error);
            });
        })
        .catch((error) => {
          console.log('Error Retrieving services', error);
        });
    }, 7000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.write(peripheral.id, '1802', '2a06', [0x03])
            .then(() => {
              console.log('SUCCESS WRITE !! CHECK BAND FOR Vibrate');
            })
            .catch((error) => {
              console.log('ERROR WRITE !! CHECK BAND FOR MESSAGE', error);
            });
        })
        .catch((error) => {
          console.log('Error Retrieving services', error);
        });
    }, 8000);

    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.write(peripheral.id, '1802', '2a06', [0x00])
            .then(() => {
              console.log('SUCCESS WRITE !! CHECK BAND FOR OFF');
            })
            .catch((error) => {
              console.log('ERROR WRITE !! CHECK BAND FOR MESSAGE', error);
            });
        })
        .catch((error) => {
          console.log('Error Retrieving services', error);
        });
    }, 9000);
  }

  handleHeartRate(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then(() => {
        BleManager.startNotification(peripheral.id, '180d', '2a37') //Starts heartBeat data fetching from miband device.
          .then(() => {
            console.log('Heart Rate notification started');
          });
      });
    }, 7000);
  }

  handleBattery(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then((services) => {
        BleManager.read(peripheral.id, 'fee0', UUID_BASE('0006'))
          .then((readData) => {
            let data = Buffer.Buffer.from(readData);
            console.log('read battery level', data.readUInt8(1, true));

            this.props.onDispatchPersistB(data.readUInt8(1, true).toString());
          })
          .catch((error) => {
            console.log('error', error);
          });
      });
    }, 1000);
  }

  handleDeviceInfo(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then((services) => {
        BleManager.read(peripheral.id, '180a', '2a28')
          .then((readData) => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Firmware Version : ', data.toString());
            this.props.onDispatchPersistF(data.toString());
          })
          .catch((error) => {
            console.log('error', error);
          });
      });
    }, 2000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then((services) => {
        BleManager.read(peripheral.id, '180a', '2a25')
          .then((readData) => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Serial Number :', data.toString());
            this.props.onDispatchPersistS(data.toString());
          })
          .catch((error) => {
            console.log('error', error);
          });
      });
    }, 3000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then((services) => {
        BleManager.read(peripheral.id, 'fee0', '2a2b')
          .then((readData) => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Time info :', parseDate(data).toString());
            this.props.onDispatchPersistT(parseDate(data).toString());
          })
          .catch((error) => {
            console.log('error', error);
          });
      });
    }, 4000);
  }

  handlePedometerStats(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.startNotification(peripheral.id, 'fee0', UUID_BASE('0007'))
            .then(() => {
              console.log(
                'Notification started for Services of Pedometer stats',
              );
            })
            .catch((error) => {
              console.log('Read Steps Error', error);
            });
        })
        .catch((error) => {
          console.log('Error ', error);
        });
    }, 5000);
  }

  renderItem(item) {
    const color = item.connected ? 'green' : '#fff';
    return (
      <TouchableHighlight onPress={() => this.test(item)}>
        <View style={[styles.row, {backgroundColor: color}]}>
          <Text
            style={{
              fontSize: 12,
              textAlign: 'center',
              color: '#333333',
              padding: 10,
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              fontSize: 10,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
            }}>
            RSSI: {item.rssi}
          </Text>
          <Text
            style={{
              fontSize: 8,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
              paddingBottom: 20,
            }}>
            {item.id}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  _handleNavigation = () => this.props.navigation.navigate('Connect');

  _handleRssi = () => this.props.onDispatchSETRSSI(23);

  render() {
    const list = Array.from(this.state.peripherals.values());
    return (
      <View style={styles.container}>
        <View style={styles.topContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this._handleNavigation()}>
            <View />
            <Text style={styles.buttonText}>Openhab Connect</Text>
            {URL ? (
              <Image
                source={require('../../assets/Images/online_openhub.png')}
                style={styles.icon}
              />
            ) : (
              <Image
                source={require('../../assets/Images/offline_openhub.png')}
                style={styles.icon}
              />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.authButton}
            onPress={() => this.props.navigation.navigate('SignIn')}>
            <Image
              source={require('../../assets/Images/authenticate.png')}
              style={styles.icon}
            />
          </TouchableOpacity>
          <View style={styles.mapViewContainer}>
            <MapHeader />
          </View>

          <TouchableHighlight
            style={styles.normalButton}
            onPress={() => this.startScan()}>
            <Text style={styles.buttonText}>
              Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.normalButton}
            onPress={() => this.retrieveConnected()}>
            {connectedTo !== 'No Device is Connected' ? (
              <Text style={styles.buttonText}>
                 Connected To: {connectedTo}{' '}
              </Text>
            ) : (
              <Text style={styles.buttonText}>
                Please Connect to a device ...
              </Text>
            )}
          </TouchableHighlight>
          <View style={styles.dataContainer}>
            <View style={styles.dataColumn}>
              <View style={styles.dataRow}>
                <Text style={styles.smallText}>Version </Text>
                <Text style={styles.value}>{fVersion}</Text>
              </View>
              <View style={styles.dataRow}>
                <Text style={styles.smallText}>Serial </Text>
                <Text style={styles.value}>{Snumber}</Text>
              </View>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/time.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{time}</Text>
              </View>
            </View>
            <View style={styles.dataColumn}>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/battery.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{battery}</Text>
              </View>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/bluetooth.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{rssi}</Text>
              </View>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/steps.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{steps}</Text>
              </View>
            </View>
            <View style={styles.dataColumn}>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/distance.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{distance}</Text>
              </View>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/calories.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{calory}</Text>
              </View>
              <View style={styles.dataRow}>
                <Image
                  source={require('../../assets/Images/heartbeat.png')}
                  style={styles.logo}
                />
                <Text style={styles.value}>{heartbeat}</Text>
              </View>
            </View>
            <ScrollView style={styles.scroll}>
              {list.length == 0 && (
                <View style={{flex: 1, margin: 20}}>
                  <Text
                    style={{textAlign: 'center', fontFamily: 'Comfortaa-Bold'}}>
                    No peripherals
                  </Text>
                </View>
              )}
              <FlatList
                data={list}
                renderItem={({item}) => this.renderItem(item)}
                keyExtractor={(item) => item.id}
              />
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  mapViewContainer: {
    width: Layout.window.width,
    height: Layout.window.height / 3,
  },
  authButton: {
    flex: 1 / 2,
    height: 50,
    marginTop: 10,
    margin: 10,
    padding: 20,
    borderRadius: 10,
    borderColor: '#f65009',
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  topContainer: {
    width: Layout.window.width,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10,
  },
  row: {
    margin: 10,
  },
  icon: {
    width: 20,
    height: 20,
  },
  dataContainer: {
    width: Layout.window.width - 50,
    height: 120,
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 10,
  },
  dataColumn: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    margin: 5,
    padding: 4,
  },
  dataRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  logo: {
    width: 20,
    height: 20,
    margin: 2,
  },
  value: {
    fontFamily: 'Comfortaa-Bold',
    fontSize: 15,
    color: 'black',
  },
  button: {
    flex: 3,
    width: Layout.window.width - 90,
    height: 50,
    marginTop: 10,
    margin: 10,
    padding: 20,
    borderRadius: 10,
    borderColor: '#f65009',
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  normalButton: {
    width: Layout.window.width - 20,
    height: 50,
    marginTop: 10,
    margin: 10,
    padding: 20,
    borderRadius: 10,
    borderColor: '#f65009',
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonText: {
    fontFamily: 'Comfortaa-Bold',
    color: '#f65009',
    fontSize: 20,
  },
  smallText: {
    fontFamily: 'Comfortaa-Bold',
    color: '#f65009',
    fontSize: 15,
  },
});

const mapStateToProps = (state) => {
  return {
    app: state.app,
    URL: state.connect.URL,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDispatchPersistR: (val) => dispatch($setRssi(val)),
    onDispatchPersistB: (val) => dispatch($setBattery(val)),
    onDispatchPersistF: (val) => dispatch($setFirmware(val)),
    onDispatchPersistS: (val) => dispatch($setSerial(val)),
    onDispatchPersistT: (val) => dispatch($setTime(val)),
    onDispatchPersistST: (val) => dispatch($setSteps(val)),
    onDispatchPersistD: (val) => dispatch($setDistance(val)),
    onDispatchPersistC: (val) => dispatch($setCalories(val)),
    onDispatchPersistH: (val) => dispatch($setHearRate(val)),
    onDispatchSETRSSI: (val) => dispatch($setRssiM(val)),
    onDispatchPersistData: () => dispatch($persistData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

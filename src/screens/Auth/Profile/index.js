import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import {connect} from 'react-redux';
import Styles from './Styles';
import {logoutUser} from '../../../actions/Auth/Actions';
import NavigationServices from '../../../Services/NavigationServices';
export class Profile extends Component {
  componentDidMount() {
    const {isAuthenticated} = this.props;
    if (!isAuthenticated) {
      this.props.navigation.navigate('SignIn');
    }
  }

  _handleLogout = () => {
    this.props.logoutUser();
    NavigationServices.navigate('SignIn');
  };

  render() {
    return (
      <View style={Styles.container}>
        <Text> You are Connected </Text>
        <TouchableOpacity onPress={() => this._handleLogout()}>
          <Text>Log out</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => dispatch(logoutUser()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

import React, {Component} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './Styles';
import {Actions} from '../../../actions/Auth';
import NavigationServices from '../../../Services/NavigationServices';
export class SignIn extends Component {
  static navigationOptions = {header: null};
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  componentDidMount() {
    const {isAuthenticated} = this.props;
    if (isAuthenticated) {
      this.props.navigation.navigate('Profile');
    }
  }

  _authenticateUser = () => {
    const {email, password} = this.state;
    console.log('state values:', email, password);
    this.props.authenticateUser(email, password);
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.isAuthenticated !== this.props.isAuthenticated &&
      prevProps.token !== this.props.token
    ) {
      this.props.navigation.navigate('Profile');
    }
  }

  render() {
    const {email, password} = this.state;
    const {isLoading, authError} = this.props;
    return (
      <KeyboardAvoidingView style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}> Mi Heart </Text>
        </View>
        <View style={styles.formContainer}>
          <TextInput
            autoFocus
            placeholder={'email'}
            underlineColorAndroid={'transparent'}
            value={email}
            onChangeText={(value) => this.setState({email: value})}
            style={styles.inputContainer}
          />
          <TextInput
            placeholder={'password'}
            underlineColorAndroid={'transparent'}
            value={password}
            secureTextEntry
            onChangeText={(value) => this.setState({password: value})}
            style={styles.inputContainer}
          />
          {authError && <Text style={styles.error}>{authError}</Text>}
          <TouchableOpacity
            onPress={() => {
              this._authenticateUser();
            }}
            style={styles.button}>
            {isLoading ? (
              <ActivityIndicator size={'large'} color={'#FFF'} />
            ) : (
              <Text style={styles.text}>Log In</Text>
            )}
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    isLoading: state.auth.isLoading,
    isAuthenticated: state.auth.isAuthenticated,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authenticateUser: (email, password) =>
      dispatch(Actions.authenticateUser(email, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);

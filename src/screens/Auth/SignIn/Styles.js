import {StyleSheet} from 'react-native';
import {Layout} from '../../../Helpers';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

    justifyContent: 'space-around',
    alignItems: 'center',
  },
  titleContainer: {
    marginHorizontal: 10,
    marginVertical: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 21,
    color: 'black',
    textAlign: 'center',
    color: '#f65009',
  },
  formContainer: {
    flex: 1,

    alignItems: 'center',
  },
  inputContainer: {
    width: Layout.window.width - 40,
    height: 50,
    padding: 10,
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#f65009',
    marginVertical: 5,
  },
  error: {
    fontSize: 20,
    color: 'red',
    textAlign: 'center',
  },
  text: {
    fontSize: 20,
    color: '#FFF',
    textAlign: 'center',
  },
  button: {
    width: Layout.window.width - 40,
    height: 50,
    borderRadius: 10,
    marginVertical: 20,
    backgroundColor: '#f65009',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

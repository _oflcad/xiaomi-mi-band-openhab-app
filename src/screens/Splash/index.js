import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';
import {Layout, Images} from '../../Helpers';
import Title from '../../components/TitleAnimated';
export default class Splash extends Component {
  static navigationOptions = {header: null};

  constructor(props) {
    super(props);

    this._goToHome = this._goToHome.bind(this);
  }

  _goToHome = () => {
    this.props.navigation.navigate('App');
  };

  render() {
    return (
      <View style={styles.container}>
        <Title />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  animation: {
    width: Layout.height / 3,
    height: Layout.height / 3,
  },
});

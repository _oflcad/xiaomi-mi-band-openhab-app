# Smart Xiaomi
![ScreenShot](/screenshots/xiaomi_screens.png)
**Smart Xiaomi** is a React Native Application that connects Xiaomi mi smart bands with your phone via bluetooth, and retrieves : 
* **Version Number**
* **Serial Number**
* **Smart Band Time**
* **Battery Level**
* **Bluetooth RSSI**
* **Steps Count**
* **Distance**
* **Calories**
* **Heart Rate**

## Application Features:
* Get User Location.
* Scan Nearby Bluetooth Devices.
* Connect To Xiaomi Mi band 3 and 4.
* Retreive datas from Device.
* Set Ip Address for **Openhab Platform**.
* Sync All the data with **Openhab Platform**.

## Future Features:
* Monitor User Heart Rate.
* IF BPM Below/Above normal rates Send SOS to nearest emergency services with user current location. 

## Installation

Navigate to android/app/release/app-release.apk for android version of the app.

## Usage (Debug mode)

```javascript
Clone Repository

cd xiaomibleapp

react-native run-android (Android)

react-native run-ios (Android)

```

## License
[Copyright © 2020 | Octopus Plus](https://www.octopus.plus/)
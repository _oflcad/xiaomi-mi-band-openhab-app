/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import AppNavigator from './src/navigation';
import {configureStore} from './src/store';
import {Provider} from 'react-redux';
import './ReactotronConfig';
import NavigationServices from './src/Services/NavigationServices';
class App extends React.Component {
  render() {
    return (
      <Provider store={configureStore()}>
        <AppNavigator
          ref={(navigatorRef) => {
            NavigationServices.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default App;
